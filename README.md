Notes
Приложение для хранения заметок в зашифрованом виде.


Приложение разработано на базе фреймворка Django и базы данных 
PostgreSQL. Заметки хранятся в зашифрованном виде с 
применением симметричного шифрования Salsa20.

Установка и запуск:
Для запуска нужно устоновить git, docker, docker-compose

Установка git:
1.  >sudo apt update
2.  >sudo apt install git -y

Установка docker:
1. >sudo apt install apt-transport-https ca-certificates curl software-properties-common
2. >curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
3. >sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
4. >sudo apt update
5. >sudo apt install docker-ce
   
Установка docker-compose:
1. >sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
2. >sudo chmod +x /usr/local/bin/docker-compose
3. >sudo usermod -aG docker $USER
   
Запуск Notes:
1. Создаем папку для приложения и переходим в нее.
2. Клонируем приложенис с Git
    >git clone https://gitlab.com/zhenyamst/notes.git
   
3. Создаем образ приложения
    >docker-compose up --build
   
Запуск тестов:
   >docker-compose run web python manage.py test
   

   
Приложение доступно по адресу http://0.0.0.0:8000

