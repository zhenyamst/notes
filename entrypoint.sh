#!/bin/sh
set -e

host="db"
port="5432"
cmd="$@"

while ! curl http://$host:$port/ 2>&1 |grep '52'
do
  sleep 1
done
python manage.py migrate
exec $cmd

