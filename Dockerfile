FROM python:3.8

ENV PYTHONUNBUFFERED=1
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
COPY ./entrypoint.sh /
RUN chmod a+x  /entrypoint.sh
RUN pip install -r requirements.txt

RUN apt-get update
RUN apt-get install -y curl
ADD . /code/

ENTRYPOINT ["/entrypoint.sh"]
