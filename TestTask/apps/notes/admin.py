from django.contrib import admin

from .models import СategoryNote, Note

admin.site.register(СategoryNote)
admin.site.register(Note)
