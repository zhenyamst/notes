from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.contrib import auth
from django.shortcuts import render, redirect
from TestTask.apps.notes.models import СategoryNote, Note
import hashlib
from Crypto.Cipher import Salsa20
import json

def encoding(plaintext, key):
    secret = hashlib.sha256(key.encode()).digest()
    cipher = Salsa20.new(key=secret)
    msg = cipher.nonce + cipher.encrypt(plaintext.encode())
    return (msg)


def decoding(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            data = json.loads(request.body.decode("utf-8"))
            msg_key = Note.objects.get(id=data['idNote']).key
            msg = Note.objects.get(id=data['idNote']).text_note
            key = data['keyNote']
            secret = hashlib.sha256(key.encode()).digest()
            msg_nonce = msg_key[:8]
            ciphertext = msg_key[8:]
            cipher = Salsa20.new(key=secret, nonce=msg_nonce)
            plaintext = cipher.decrypt(ciphertext)
            try:
                password = plaintext.decode()
                if password == key:
                    msg_nonce = msg[:8]
                    ciphertext = msg[8:]
                    cipher = Salsa20.new(key=secret, nonce=msg_nonce)
                    plaintext = cipher.decrypt(ciphertext)
                    return HttpResponse(plaintext)
                else:
                    return HttpResponse('None')
            except:
                return HttpResponse('None')

def list_notes(request, category_id):
    if request.user.is_authenticated:
        category = СategoryNote.objects.get(id=category_id)
        if request.user == category.user:
            list_notes = category.note_set.all()
            return render(request, "notes/notes.html", {"list": list_notes, "id_category": category_id})
        else:
            raise Http404("В доступе отказано")

    else:
        return redirect("/login/")


def notes_detail(request, category_id, notes_id):
    if category_id:
        a = Note.objects.get(id=notes_id)
        a.text_note = a.text_note.tobytes()
    return render(request, "notes/notes_detail.html", {"result": a, "flag_add": False})


def add_note(request, category_id):
    if request.user.is_authenticated:
        if request.method == "POST" and bool(category_id):
            categoryAdd = СategoryNote.objects.get(id=category_id)
            add = Note(category=categoryAdd, name_note=request.POST['name_note'],
                       text_note=encoding(request.POST['text_note'], request.POST['password']),
                       key=encoding(request.POST['password'], request.POST['password']))
            add.save()
            return redirect("/category/" + str(category_id))
        else:
            return render(request, "notes/notes_detail.html", {"result": None, "flag_add": True})
    else:
        return redirect("/")


def delete_note(request, category_id, notes_id):
    if request.user.is_authenticated:
        try:
            note = Note.objects.get(id=notes_id)
            note.delete()
            return redirect("/category/" + str(category_id))
        except:
            raise Http404('Заметка не найдена')
    else:
        return redirect("/")


def edit_note(request, category_id, notes_id):
    if request.user.is_authenticated:
        if request.method == "POST" and bool(category_id) and bool(notes_id):
            note = Note.objects.get(id=notes_id)
            note.name_note = request.POST['name_note']
            note.text_note = encoding(request.POST['text_note'], request.POST['password'])
            note.key = encoding(request.POST['password'], request.POST['password'])
            note.save()
            return redirect("/category/" + str(category_id))
    else:
        return redirect("/")
