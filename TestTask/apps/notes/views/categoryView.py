from django.http import Http404, HttpResponseRedirect
from django.contrib import auth
from django.shortcuts import render, redirect
from TestTask.apps.notes.models import СategoryNote, Note


def index(request):
    if request.user.is_authenticated:
        list_category = СategoryNote.objects.filter(user=request.user)
        return render(request, "notes/category.html", {"title_block": "Категории", "list": list_category})
    else:
        return redirect("/login/")


def add_category(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            add = СategoryNote(id=None, user=auth.get_user(request), category_title=request.POST['name'])
            add.save()
        list_category = СategoryNote.objects.all()
        return redirect("/")
    else:
        return redirect("/")


def edit_category(request):
    if request.user.is_authenticated:
        try:
            category = СategoryNote.objects.get(id=request.POST['id_category'])
            category.category_title = request.POST['name_category']
            category.save()
            return HttpResponseRedirect("/")
        except:
            return HttpResponseRedirect("/")
    else:
        return redirect("/")


def delete_category(request, id):
    if request.user.is_authenticated:
        try:
            category = СategoryNote.objects.get(id=id)
            category.delete()
            return HttpResponseRedirect("/")
        except:
            raise Http404('Категория не найдена')
    else:
        return redirect("/")
