from django.urls import path
from TestTask.apps.notes.views import categoryView, noteView

app_name = 'notes'

urlpatterns = [
    path('', categoryView.index, name='index'),
    path('add_category/', categoryView.add_category, name='add_category'),
    path('delete_category/<int:id>/', categoryView.delete_category, name='delete_category'),
    path('category/edit/', categoryView.edit_category, name='edit_category'),

    path('category/<int:category_id>/', noteView.list_notes, name='list_notes'),
    path('category/<int:category_id>/<int:notes_id>/', noteView.notes_detail, name='notes_detail'),
    path('category/<int:category_id>/add_note/', noteView.add_note, name='add_note'),
    path('category/<int:category_id>/<int:notes_id>/delete_note', noteView.delete_note, name='delete_note'),
    path('category/<int:category_id>/<int:notes_id>/edit_note', noteView.edit_note, name='edit_name'),

    path('encoding/', noteView.encoding, name='encoding'),
    path('decoding/', noteView.decoding, name='decoding'),

]