from django.test import TestCase, RequestFactory
from django.contrib.auth.models import User
from TestTask.apps.notes.models import СategoryNote, Note
from TestTask.apps.notes.views import add_note

class TestCase(TestCase):
    def setUp(self)->None:
        self.user = User.objects.create_user('testuser', 'test@test.com', 'password')
        self.category = СategoryNote.objects.create(
            user=self.user,
            category_title='testCategory',
        )



    def test_index_loads_properly(self):
        print("fsdfsdefsd")
        response = self.client.get('/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/add_category/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/delete_category/'+str(self.category.id)+'/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('category/'+str(self.category.id)+'/')
        self.assertEqual(response.status_code, 404)
        response = self.client.get('category/'+str(self.category.id)+'/add_note/')
        self.assertEqual(response.status_code, 404)


    def test_post_add_note(self):
        factory = RequestFactory()
        request = factory.post('category/'+str(self.category.id)+'/add_note/', {
            'name_note':'test', 'text_note':'text_test', 'password':'test'
        })
        request.user = self.user
        response = add_note(request, category_id=self.category.id)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, '/category/' + str(self.category.id))

        self.assertFalse(Note.objects.get(name_note='test').text_note == 'text_test' )





