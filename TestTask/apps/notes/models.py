from django.db import models
from django.contrib.auth.models import User


class СategoryNote(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="notes", null=True)
    category_title = models.CharField('Название заметки', max_length=200)

    def __str__(self):
        return self.category_title


class Note(models.Model):
    category = models.ForeignKey(СategoryNote, on_delete=models.CASCADE)
    name_note = models.CharField('Название заметки', max_length=200)
    text_note = models.BinaryField('Текст заметки')
    key = models.BinaryField('Ключ шифрования')

    def __str__(self):
        return self.name_note
