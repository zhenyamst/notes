from django.apps import AppConfig


class RegisterConfig(AppConfig):
    name = 'TestTask.apps.register'
