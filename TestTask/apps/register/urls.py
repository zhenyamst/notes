from django.urls import path
from . import views
from django.contrib.auth import views as loginView

app_name = 'register'

urlpatterns = [
    path('register/', views.register, name='register'),
    path('', loginView.LoginView.as_view(), name='login'),
    path('logout/', loginView.LogoutView.as_view(), name='logout')

]